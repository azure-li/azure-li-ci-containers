#!/bin/sh

image=$1
startdir=$2
registry=$3
project=$4
master_build=0

if test "$CI_PROJECT_NAMESPACE" = "azure-li" && test "$CI_PROJECT_NAME" = "azure-li-ci-containers"; then
    master_build=1
fi
set -e
docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN $registry
docker build --no-cache -t $registry/$project:$image $startdir
if test $master_build = 0;then
    echo "Not a master build"
    exit 0
else
    docker push $registry/$project:$image
fi
